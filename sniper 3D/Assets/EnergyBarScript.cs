﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBarScript : MonoBehaviour
{
    [SerializeField]
    private float  fillAmount ;
    [SerializeField]
    private Image content;
    // Start is called before the first frame update
    void Start()
    {
      //  HandleBar();
    }

    // Update is called once per frame
    void Update()
    {
        HandleBar();
    }
    private void HandleBar()
    {
        content.fillAmount = fillAmount;
    }
    private float Map(float value, float inMax , float outMin , float outMax)
    {
        return (value / outMax);
    }
}
