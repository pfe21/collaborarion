﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementManager : MonoBehaviour
{
    public Button buttonComponent;
    public Text content;
    public Image iconImage;
    private Achievement achievement;
    private AchievementsList scrollList;
    public void Setup(Achievement currentAchievement, AchievementsList currentScrollList)
    {
        achievement = currentAchievement;
        content.text = achievement.achievementName;
        iconImage.sprite = achievement.icon;
        scrollList = currentScrollList;

    }


}