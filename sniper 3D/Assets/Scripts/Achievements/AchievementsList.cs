﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class Achievement
{
    public string achievementName;
    public Sprite icon;
    
}

public class AchievementsList : MonoBehaviour
{
    public List<Achievement> achievementList;
    public Transform contentPanel;
    public AchievementPool objectPool;
    void Start()
    {
        RefreshDisplay();
    }

    void RefreshDisplay()
    {

        AddButtons();
    }



    private void AddButtons()
    {
        for (int i = 0; i < achievementList.Count; i++)
        {
            Achievement achievement = achievementList[i];
            GameObject newAch = objectPool.GetObject();
            newAch.transform.SetParent(contentPanel);

            AchievementManager  ach = newAch.GetComponent<AchievementManager>();
            ach.Setup(achievement, this);
        }
    }



    void Addachievement(Achievement achievementToAdd, AchievementsList achList)
    {
       achList.achievementList.Add(achievementToAdd);
    }


}