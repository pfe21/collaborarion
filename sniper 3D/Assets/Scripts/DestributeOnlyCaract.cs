﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestributeOnlyCaract : MonoBehaviour
{
    public Text[] prop;
    public Text[] propVal;
    public Image[] mySlide;
    public Text[] propValMax;
    public GameObject PlayPanel;
    public GameObject mainMenu;
    public void DoleOut(SnipersData caract)
    {
        
        PlayPanel.SetActive(true);
        mainMenu.SetActive(false);
        int i = 0;
        if (tag.Equals(caract.sniperName))
        {
            foreach (var p in prop)
            {
                p.text = caract.propList[i].propName;
                i++;
            }
            int j = 0;
            foreach (var x in propVal)
            {
                x.text = caract.propList[j].val.ToString();
                j++;
            }
        }
    }
}
