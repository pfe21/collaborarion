﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Mission
{
    public int index;
    public string content;

}

public class MissionScrollList : MonoBehaviour
{
    public List<Mission> missionList;
    public Transform contentPanel;
    public MissionPool buttonObjectPool;
    void Start()
    {
        RefreshDisplay();
    }

    void RefreshDisplay()
    {

        AddMissions();
    }



    private void AddMissions()
    {
        for (int i = 0; i < missionList.Count; i++)
        {
            Mission item = missionList[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel);

            MissionManager mission = newButton.GetComponent<MissionManager>();
            mission.Setup(item, this);
        }


    }
}