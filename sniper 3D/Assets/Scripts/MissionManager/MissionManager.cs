﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionManager : MonoBehaviour
{
    public Text missionNb;
    public Text missionContent;
    private Mission mission;
    private MissionScrollList missionList;

    public void Setup(Mission currentMission, MissionScrollList currentMissionList)
    {
        mission = currentMission;
        missionNb.text = mission.index.ToString();
        missionContent.text = mission.content;
        missionList = currentMissionList;
    }

}
