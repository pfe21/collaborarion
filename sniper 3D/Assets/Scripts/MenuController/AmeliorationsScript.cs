﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmeliorationsScript : MonoBehaviour
{
    public void openPanel(GameObject _object)
    {
        _object.SetActive(true);
    }
    public void ClosePanel(GameObject _object)
    {
        _object.SetActive(false);
    }
}
