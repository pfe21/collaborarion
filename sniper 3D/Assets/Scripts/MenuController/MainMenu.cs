﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject storeMenu;
    public GameObject achievementsMenu;
    public GameObject optionsMenu;
    public GameObject playMenu;
    public GameObject caractMenu;
    public GameObject ameliorationsMenu;
    public GameObject missionsMenu;
    public GameObject background;
    public GameObject profileMenu;
   // public GameObject plane;
    public GameObject currentSniper;

    public void DisableAll()
    {
        mainMenu.SetActive(false);
        storeMenu.SetActive(false);
        achievementsMenu.SetActive(false);
        optionsMenu.SetActive(false);
        playMenu.SetActive(false);
        caractMenu.SetActive(false);
        ameliorationsMenu.SetActive(false);
        missionsMenu.SetActive(false);
        background.SetActive(false);
        profileMenu.SetActive(false);
       // plane.SetActive(false);
        currentSniper.SetActive(false);


    }
    public void GoBack()
    {
         if(mainMenu.activeInHierarchy)
        {
            DisableAll();
            Quit();
        }
        if (storeMenu.activeInHierarchy)
        {
            DisableAll();
            mainMenu.SetActive(true);
            background.SetActive(true);
        }
        if (achievementsMenu.activeInHierarchy)
        {
            DisableAll();
            mainMenu.SetActive(true);
            background.SetActive(true);
        }
        if (playMenu.activeInHierarchy)
        {
            DisableAll();
            mainMenu.SetActive(true);
            background.SetActive(true);
        }
        if (caractMenu.activeInHierarchy)
        {
            DisableAll();
            storeMenu.SetActive(true);
            background.SetActive(true);
        }
        if (ameliorationsMenu.activeInHierarchy)
        {
            DisableAll();
            playMenu.SetActive(true);
          //  plane.SetActive(true);
            currentSniper.SetActive(true);
        }
        if (missionsMenu.activeInHierarchy)
        {
            DisableAll();
            playMenu.SetActive(true);
           // plane.SetActive(true);
            currentSniper.SetActive(true);
        }     
        if (profileMenu.activeInHierarchy)
        {
            DisableAll();
            mainMenu.SetActive(true);
            background.SetActive(true);
        }
        if(optionsMenu.activeInHierarchy)
        {
            DisableAll();
            mainMenu.SetActive(true);
            background.SetActive(true);
        }
    }
    public void PlayGame()
    {
      SceneManager.LoadScene(1);
    }
    public void openPanel(GameObject _object)
    {
        _object.SetActive(true);
    }
    public void ClosePanel( GameObject _object)
    {
        _object.SetActive(false);
    }
    public void Quit()
    {
        Debug.Log("QUIT THE APP!");
        Application.Quit();
    }
}
