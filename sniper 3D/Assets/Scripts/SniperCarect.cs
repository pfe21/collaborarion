﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SniperCarect : MonoBehaviour
{
    // Start is called before the first frame updateT
    public TextMeshProUGUI SniperNameDisplayText;
    public Text SniperPriceDisplayText;
    public Text[] prop;
    public Text[] propVal;
    public Text[] propValMax;
    public Image[] mySlide;
    public GameObject storePanel;
    public GameObject dsr;
    public GameObject m2oo;
    public GameObject huntingRifle;
    public GameObject sniper4;
    public GameObject background;
    public GameObject carct;
    public void DisabllAll()
    {
        carct.SetActive(false);
        storePanel.SetActive(false);
        background.SetActive(false);
        dsr.SetActive(false);
        m2oo.SetActive(false);
        huntingRifle.SetActive(false);
        sniper4.SetActive(false);
    }
    
    public void Destrebute(SnipersData data)
    {
        {
            if (data.sniperName == "DSR-1")
            {
                DisabllAll();
                carct.SetActive(true);
                //storePanel.SetActive(false);
                //background.SetActive(false);
                dsr.SetActive(true);
                SniperNameDisplayText.text = data.sniperName;
                SniperPriceDisplayText.text = data.price.ToString();
                int i = 0;
                foreach (var p in prop)
                {
                    p.text = data.propList[i].propName;
                    i++;
                }
                int j = 0;
                foreach (var x in propVal)
                {
                    x.text = data.propList[j].val.ToString();
                    j++;
                }
                int z = 0;
                foreach (var max in propValMax)
                { 
                    max.text = data.propList[z].maxVal.ToString();
                    z++;
                }
                int k = 0;
                foreach (var slider in mySlide)
                {
                    if (data.propList[k].maxVal > data.propList[k].val)
                    { 
                       slider.fillAmount = data.propList[k].val / data.propList[k].maxVal;
                      
                    }
                    else
                    {
                        slider.fillAmount = data.propList[k].maxVal / data.propList[k].val;
                    }
                    k++;
                }

            }
            else if (data.sniperName == "HUNTING RIFLE")
            {
                DisabllAll();
                carct.SetActive(true);
                //storePanel.SetActive(false);
                //background.SetActive(false);
                huntingRifle.SetActive(true);
                SniperNameDisplayText.text = data.sniperName;
                SniperPriceDisplayText.text = data.price.ToString();
                int i = 0;
                foreach (var p in prop)
                {
                    p.text = data.propList[i].propName;
                    i++;
                }
                int j = 0;
                foreach (var x in propVal)
                {
                    x.text = data.propList[j].val.ToString();
                    j++;
                }
                int z = 0;
                foreach (var max in propValMax)
                {
                    max.text = data.propList[z].maxVal.ToString();
                    z++;
                }
                int k = 0;
               foreach (var slider in mySlide)
                {
                     if (data.propList[k].maxVal >= data.propList[k].val)
                     { 
                        slider.fillAmount = data.propList[k].val / data.propList[k].maxVal;
                        // return;
                     }
                     else if (data.propList[k].maxVal < data.propList[k].val)
                     {
                         slider.fillAmount = data.propList[k].maxVal / data.propList[k].val;
                     }
                    //slider.fillAmount = data.propList[k].val / data.propList[k].maxVal;
                    k++;
                }
              

            }
            else if (data.sniperName == "M200")
            {
                DisabllAll();
                carct.SetActive(true);
               // storePanel.SetActive(false);
               // background.SetActive(false);
                m2oo.SetActive(true);
                SniperNameDisplayText.text = data.sniperName;
                SniperPriceDisplayText.text = data.price.ToString();
                int i = 0;
                foreach (var p in prop)
                {
                    p.text = data.propList[i].propName;
                    i++;
                }
                int j = 0;
                foreach (var x in propVal)
                {
                    x.text = data.propList[j].val.ToString();
                   j++;
                }
                int z = 0;
                foreach (var max in propValMax)
                {
                    max.text = data.propList[z].maxVal.ToString();
                   z++;
                }
                int k = 0;
                foreach (var slider in mySlide)
                {
                    if (data.propList[k].maxVal > data.propList[k].val)
                    {
                        slider.fillAmount = data.propList[k].val / data.propList[k].maxVal;
                       
                    }
                    else
                    {
                        slider.fillAmount = data.propList[k].maxVal / data.propList[k].val;
                    }
                    k++;
                }

            }
            else if (data.sniperName == "sniper4")
            {
                DisabllAll();
                carct.SetActive(true);
               // storePanel.SetActive(false);
                //background.SetActive(false);
                sniper4.SetActive(true);
                SniperNameDisplayText.text = data.sniperName;
                SniperPriceDisplayText.text = data.price.ToString();
                int i = 0;
                foreach (var p in prop)
                {
                    p.text = data.propList[i].propName;
                    i++;
                }
                int j = 0;
                foreach (var x in propVal)
                {
                    x.text = data.propList[j].val.ToString();
                    j++;
                }
                int z = 0;
                foreach (var max in propValMax)
                {
                    max.text = data.propList[z].maxVal.ToString();
                    z++;
                }
                int k = 0;
                foreach (var slider in mySlide)
                {
                    if (data.propList[k].maxVal > data.propList[k].val)
                    {
                        slider.fillAmount = data.propList[k].val / data.propList[k].maxVal;
                       
                    }
                    else
                    {
                        slider.fillAmount = data.propList[k].maxVal / data.propList[k].val;
                    }
                    k++;
                }

            }
        }

      
    }
    
}
