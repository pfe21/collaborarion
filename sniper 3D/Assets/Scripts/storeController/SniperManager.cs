﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SniperManager : MonoBehaviour
{
    public Button buttonComponent;
    public Text nameLabel;
    public Image iconImage;
    public Text priceText;
    private Item item;
    private ShopScrollList scrollList;

   public SniperTable sniper;
    public void Setup(Item currentItem, ShopScrollList currentScrollList)
    {
        item = currentItem;
        nameLabel.text = item.itemName;
        iconImage.sprite = item.icon;
        priceText.text = item.price.ToString();
        scrollList = currentScrollList;
    }
    public void GetItem ()
    {
         sniper = DataControlller.tab; 
        if(sniper!= null)
        {
            foreach (var item in sniper.allData)
            {
                if (item.sniperName==nameLabel.text)
                {
                    GameObject Canvas = GameObject.Find("Canvas");
                    Canvas.GetComponent<SniperCarect>().Destrebute(item);
                    return;
                }
            }
        }
    }
    public void Destribute ()
    {

        GetItem();
  
    }
}