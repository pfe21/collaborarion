﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class Item
{
    public string itemName;
    public Sprite icon;
    public float price = 1;
}

public class ShopScrollList : MonoBehaviour
{ 
   public List<Item> itemList;
    public Transform contentPanel;
    public SniperPool buttonObjectPool;
    void Start()
    {
        RefreshDisplay();
    }

    void RefreshDisplay() { 
    
        AddButtons();
    }

  

    private void AddButtons()
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            Item item = itemList[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel);

            SniperManager sniper = newButton.GetComponent<SniperManager>();
            sniper.Setup(item, this);
        }
    }

    

    void AddItem(Item itemToAdd, ShopScrollList shopList)
    {
        shopList.itemList.Add(itemToAdd);
    }

   
}