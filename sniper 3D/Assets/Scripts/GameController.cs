﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text scoreDisplayText;
    public GameObject window;
    public GameObject window1;
    public Button buy;
    public Button yes;
    private DataControlller dataControlller;
    private SniperTable currentSniper;
    private SnipersData sniper;
    private int score;
    // Start is called before the first frame update
    void Start()
    {
        dataControlller = FindObjectOfType<DataControlller>();
        score = 30000;
        setScoreText();
        buy.onClick.AddListener(buyButton);
        yes.onClick.AddListener(YesButtonClicked);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void buyButton()
    {
        print("coucou");
        BuyButtonClicked(sniper);
    }
    public void BuyButtonClicked(SnipersData snipers)
    {
        print("heyyyyy");
        if (snipers.price  <= score)
        {
            window.SetActive(true);
            return;
        }
        else if (snipers.price > score)
        {
            window1.SetActive(true);
            return;
        }
    }
    public void YesButtonClicked()
    {
        score = score - sniper.price;
        Destroy(buy);
        window.SetActive(false);
        setScoreText();
    }
    public void setScoreText()
    {
        scoreDisplayText.text = score.ToString();
    }
}
