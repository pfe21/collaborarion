﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradeManager : MonoBehaviour
{
    public Image ImageComponent;
    public Text nameLabel;
    public Image iconImage;
    private GradeList myGrade;
    private Grade grade;
  
 
    public void Setup(Grade currentGrade, GradeList currentGradeList)
    {
        grade = currentGrade;
        nameLabel.text = grade.itemName;
        iconImage.sprite = grade.icon;
        myGrade = currentGradeList;
        
    }

}
