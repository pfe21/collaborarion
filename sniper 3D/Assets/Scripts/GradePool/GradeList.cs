﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Grade
{
    public string itemName;
    public Sprite icon;
  
}

public class GradeList : MonoBehaviour
{

    public List<Grade> gradeList;
    public Transform contentPanel;
    public GradePool buttonObjectPool;
    void Start()
    {
        RefreshDisplay();
    }

    void RefreshDisplay()
    {

        AddButtons();
    }



    private void AddButtons()
    {
        for (int i = 0; i < gradeList.Count; i++)
        {
            Grade item = gradeList[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel);

            GradeManager grade= newButton.GetComponent<GradeManager>();
            grade.Setup(item, this);
        }
    }



    void AddItem(Item itemToAdd, ShopScrollList shopList)
    {
        shopList.itemList.Add(itemToAdd);
    }

}
