﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataControlller : MonoBehaviour
{
    public SniperTable snipers;
    public static SniperTable tab;
    public SnipersData[] allData;
    private string gameDataFileName = "file.json";
    void Start()
    {
        LoadGameData();
        DontDestroyOnLoad(gameObject);
        SceneManager.LoadScene("MenuScene");
    }
    public SnipersData GetCurrentSniperData()
    {
        return allData[0];
    }
    private void LoadGameData()
    {
        // Path.Combine combines strings into a file path
        // Application.StreamingAssets points to Assets/StreamingAssets in the Editor, and the StreamingAssets folder in a build
        //string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);
        //string filePath = Path.Combine(Application.dataPath, gameDataFileName);
        string filePath = JsonFileReader.LoadJsonAsResource("file.json");
        print(filePath);
        //TextAsset file = Resources.Load<TextAsset>("file");
        //filePath = file.text;
       
            // Read the json from the file into a string
           // string dataAsJson = File.ReadAllText(filePath);
            // Pass the json to JsonUtility, and tell it to create a GameData object from it
            SniperTable loadedData = JsonUtility.FromJson<SniperTable>(filePath);
            // Retrieve the allRoundData property of loadedData
            snipers = loadedData;
            tab = snipers;
            print(allData);
          
        
    }
}
